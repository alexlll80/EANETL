USE [UpstreamDataSupply]
GO
/****** Object:  User [UpstreamSupplier]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE USER [UpstreamSupplier] FOR LOGIN [UpstreamDataSupplier] WITH DEFAULT_SCHEMA=[etl]
GO
/****** Object:  DatabaseRole [UpstreamSuppliers]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE ROLE [UpstreamSuppliers]
GO
/****** Object:  Schema [acm]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE SCHEMA [acm]
GO
/****** Object:  Schema [acrawcache]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE SCHEMA [acrawcache]
GO
/****** Object:  Schema [dev]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE SCHEMA [dev]
GO
/****** Object:  Schema [ean]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE SCHEMA [ean]
GO
/****** Object:  Schema [eanraw]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE SCHEMA [eanraw]
GO
/****** Object:  Schema [etl]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE SCHEMA [etl]
GO
/****** Object:  Schema [geo]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE SCHEMA [geo]
GO
/****** Object:  Schema [iata]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE SCHEMA [iata]
GO
/****** Object:  Schema [iso]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE SCHEMA [iso]
GO
/****** Object:  Schema [pop]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE SCHEMA [pop]
GO
/****** Object:  Schema [sau]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE SCHEMA [sau]
GO
/****** Object:  Schema [ssm]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE SCHEMA [ssm]
GO
/****** Object:  Schema [std]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE SCHEMA [std]
GO
/****** Object:  Schema [tmp]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE SCHEMA [tmp]
GO
/****** Object:  Schema [uti]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE SCHEMA [uti]
GO
/****** Object:  Schema [xxx]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE SCHEMA [xxx]
GO
/****** Object:  UserDefinedDataType [dbo].[BriefNote]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE TYPE [dbo].[BriefNote] FROM [varchar](255) NULL
GO
/****** Object:  UserDefinedDataType [dbo].[BriefNoteN]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE TYPE [dbo].[BriefNoteN] FROM [nvarchar](255) NULL
GO
/****** Object:  UserDefinedDataType [dbo].[HugeNote]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE TYPE [dbo].[HugeNote] FROM [varchar](max) NULL
GO
/****** Object:  UserDefinedDataType [dbo].[HugeNoteN]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE TYPE [dbo].[HugeNoteN] FROM [nvarchar](max) NULL
GO
/****** Object:  UserDefinedDataType [dbo].[IataCode]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE TYPE [dbo].[IataCode] FROM [char](3) NULL
GO
/****** Object:  UserDefinedDataType [dbo].[IsoCountry]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE TYPE [dbo].[IsoCountry] FROM [char](2) NULL
GO
/****** Object:  UserDefinedDataType [dbo].[IsoCountryCaption]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE TYPE [dbo].[IsoCountryCaption] FROM [varchar](64) NULL
GO
/****** Object:  UserDefinedDataType [dbo].[IsoCurrency]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE TYPE [dbo].[IsoCurrency] FROM [char](3) NULL
GO
/****** Object:  UserDefinedDataType [dbo].[TxAlias]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE TYPE [dbo].[TxAlias] FROM [nvarchar](40) NULL
GO
/****** Object:  UserDefinedDataType [dbo].[TxCaption]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE TYPE [dbo].[TxCaption] FROM [varchar](40) NOT NULL
GO
/****** Object:  UserDefinedDataType [dbo].[TxCode]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE TYPE [dbo].[TxCode] FROM [varchar](24) NULL
GO
/****** Object:  UserDefinedDataType [dbo].[URL]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE TYPE [dbo].[URL] FROM [nvarchar](300) NULL
GO
/****** Object:  UserDefinedTableType [dbo].[tInteger]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE TYPE [dbo].[tInteger] AS TABLE(
	[n] [int] IDENTITY(0,1) NOT NULL,
	[x] [int] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[tNumberedStringPair]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE TYPE [dbo].[tNumberedStringPair] AS TABLE(
	[n] [int] IDENTITY(0,1) NOT NULL,
	[t] [sysname] NOT NULL,
	[x] [sysname] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[tRoomPax]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE TYPE [dbo].[tRoomPax] AS TABLE(
	[n] [int] IDENTITY(0,1) NOT NULL,
	[Adults] [int] NULL,
	[ChildrenAges] [varchar](10) NULL,
	[Children]  AS (len(isnull([ChildrenAges],(0))))
)
GO
/****** Object:  UserDefinedTableType [dbo].[ttCurrency]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE TYPE [dbo].[ttCurrency] AS TABLE(
	[n] [int] IDENTITY(0,1) NOT NULL,
	[Ccy] [char](3) NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[ttCurrencyRateFeed]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE TYPE [dbo].[ttCurrencyRateFeed] AS TABLE(
	[n] [int] IDENTITY(0,1) NOT NULL,
	[Place] [int] NOT NULL,
	[Sell] [char](3) NOT NULL,
	[Buy] [char](3) NOT NULL,
	[Quoted] [date] NOT NULL,
	[Value] [decimal](18, 8) NOT NULL
)
GO
/****** Object:  UserDefinedFunction [acrawcache].[sfSearchKeyCompressed]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
SELECT acrawcache.sfSearchKeyCompressed('MOW_RHO_25_09_2016_0_0_RHO_MOW_04_10_2016_0_0_E_2_0_1_0_0_9')
SELECT acrawcache.sfSearchKeyCompressed('MOW_SGN_14_11_2016_0_0_HAN_BKK_25_11_2016_0_0_BKK_MOW_28_11_2016_0_0_B_2_0_0_0_0_9')
*/
CREATE FUNCTION [acrawcache].[sfSearchKeyCompressed](@RawKey varchar(200))
RETURNS varchar(128)
AS
BEGIN
DECLARE
  @Route1 varchar(200) = LEFT(@RawKey, 22)
, @Route2 varchar(200) = SUBSTRING(@RawKey, 24, 23)
, @Route3 varchar(200) = SUBSTRING(@RawKey, 47, 23)
, @Route4 varchar(200) = SUBSTRING(@RawKey, 70, 23)
, @Tail   varchar(200) = RIGHT(@RawKey, 15)
;

RETURN
  REPLACE(REPLACE(@Route1, '_0_0', ''), '_', '')
+ STUFF(REPLACE(@Tail, '_', ''), 1, 1, '-')
+ CASE WHEN LEN(@Route2) < 23 THEN '' ELSE '~' + REPLACE(REPLACE(@Route2, '_0_0', ''), '_', '') END
+ CASE WHEN LEN(@Route3) < 23 THEN '' ELSE '~' + REPLACE(REPLACE(@Route3, '_0_0', ''), '_', '') END
+ CASE WHEN LEN(@Route4) < 23 THEN '' ELSE '~' + REPLACE(REPLACE(@Route4, '_0_0', ''), '_', '') END
;
END


GO
/****** Object:  UserDefinedFunction [dbo].[sfAboveZ]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- SELECT dbo.sfAboveZ(10), dbo.sfAboveZ(-10)
CREATE FUNCTION [dbo].[sfAboveZ](@V int)
RETURNS int
AS BEGIN
RETURN
  CASE
    WHEN @V > 0 THEN @V
    ELSE 0
  END
END
GO
/****** Object:  UserDefinedFunction [dev].[tCodeDependency]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--view, procedure references table, view, procedure
--procedure references type
--table(check) references procedure
--trigger references table, procedure
--
-- select * from dev.tCodeDependency()
CREATE FUNCTION [dev].[tCodeDependency]()
RETURNS @t TABLE(
parent_name	nvarchar(514),
child_name	nvarchar(514),
parent_id	int,
parent_type	varchar(2),
child_id	int,
child_type	char(2),
class	tinyint,
tier	int
)
AS BEGIN
insert @t(
  parent_name,
  child_name,
  parent_id,
  parent_type,
  child_id,
  child_type,
  class,
  tier
)
select
  schema_name(pa.schema_id) + '.' + pa.name parent_name,
  schema_name(obj.schema_id) + '.' + obj.name child_name,
  pa.object_id parent_id,
  rtrim(pa.type) parent_type,
  dp.object_id child_id,
  rtrim(obj.type) child_type,
  dp.class,
  cast(0 as int) tier
from sys.all_objects AS pa
join (
  select
    dp.class,
    dp.object_id,
    dp.referenced_major_id
  from sys.sql_dependencies as dp
  where dp.class < 2
union
  select
    referencing_class class,
    referencing_id object_id,
    dp.referenced_id referenced_major_id
  from sys.sql_expression_dependencies dp  
) as dp
 on dp.referenced_major_id = pa.object_id
and pa.is_ms_shipped = 0
and dp.class < 2
and pa.type not in ('U')
--and pa.type in ('U', 'C', 'V', 'P', 'RF', 'PC', 'TF', 'FN', 'IF', 'FS', 'FT')
--and pa.type in ('TF', 'FN', 'IF', 'FS', 'FT')
join sys.objects as obj
 on obj.object_id = dp.object_id
and obj.is_ms_shipped = 0
--and obj.type in ( 'U', 'C', 'V', 'P', 'RF', 'PC', 'TF', 'FN', 'IF', 'FS', 'FT', 'TR')
--where pa.type = 'V' and obj.type = 'FN'

declare @Rows int, @Tier int;
select @Rows = -1, @Tier = 0;

WHILE @Rows <> 0 BEGIN
  insert @t (
    parent_name,
    child_name,
    parent_id,
    parent_type,
    child_id,
    child_type,
    class,
    tier
  )
  select
    pa.parent_name,
    ch.child_name,
    pa.parent_id,
    pa.parent_type,
    ch.child_id,
    ch.child_type,
    ch.class,
    ch.tier + 1
  from @t pa
  join @t ch
    on pa.child_id = ch.parent_id
   and ch.tier = 0
   and pa.tier = @Tier;
   
   select
    @Rows = @@ROWCOUNT,
    @Tier += 1;
    
END

RETURN
END

GO
/****** Object:  UserDefinedFunction [dev].[tTableDependency]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- select * from dev.tTableDependency()
CREATE FUNCTION [dev].[tTableDependency]()
RETURNS @t TABLE(
ParentTableName	nvarchar(514),
ChildTableName  nvarchar(514),
FKName nvarchar(514),
key_index_id int,
OnDelete nvarchar(514),
OnUpdate nvarchar(514),
is_disabled bit,
is_not_trusted bit,
ChildTableId	int,
ParentTableId int
)
AS BEGIN
insert @t(
  ParentTableName,
  ChildTableName,
  FKName,
  key_index_id,
  OnDelete,
  OnUpdate,
  is_disabled,
  is_not_trusted,
  ChildTableId,
  ParentTableId
)
select
  SCHEMA_NAME(ro.schema_id) + '.' +  ro.name ParentTableName,
  SCHEMA_NAME(childtable.schema_id) + '.' +  childtable.name ChildTableName,
  fk.name FKName,
  fk.key_index_id,
  fk.delete_referential_action_desc OnDelete,
  fk.update_referential_action_desc OnUpdate,
  fk.is_disabled,
  fk.is_not_trusted,
  childtable.object_id ChildTableId,
  ro.object_id ParentTableId
--  ,fk.*
--  ,childtable.*
from sys.foreign_keys fk
join sys.objects childtable
  on fk.parent_object_id = childtable.object_id
 and childtable.is_ms_shipped = 0
 and fk.is_ms_shipped = 0
join sys.objects ro
  on fk.referenced_object_id = ro.object_id
order by 1, 2

RETURN
END

GO
/****** Object:  UserDefinedFunction [std].[sfChristMonth]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
DECLARE @XP smallint = std.sfChristMonth(-2730, 6);
SELECT
  @XP iXP
, @XP/12 + SIGN(@XP) Y
, ABS(@XP%12) M
;
DECLARE @XP smallint = std.sfChristMonth(2731, 6);
SELECT
  @XP iXP
, @XP/12 + SIGN(@XP) Y
, @XP%12 M
;
*/
CREATE FUNCTION [std].[sfChristMonth](@Year smallint, @Month tinyint)
RETURNS smallint
AS BEGIN
RETURN (@Year - SIGN(@Year)) * 12 + SIGN(@Year)*(@Month - 1);
END
GO
/****** Object:  UserDefinedFunction [uti].[sfDateFromParts]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- SELECT uti.sfDateFromParts(16, 1, 1)
CREATE FUNCTION [uti].[sfDateFromParts](@Y smallint, @M tinyint, @D tinyint)
RETURNS date
AS BEGIN
RETURN REPLACE(Str(@Y, 4) + Str(@M, 2) + Str(@D, 2), ' ', '0')
END

GO
/****** Object:  UserDefinedFunction [uti].[sfEoMonth]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- SELECT uti.sfEoMonth('20160215')
CREATE FUNCTION [uti].[sfEoMonth](@d date)
RETURNS date
AS BEGIN
DECLARE @dd date = DATEADD(mm, 1, @D);
RETURN DATEADD(dd, - DATEPART(dd, @dd), @dd);
END

GO
/****** Object:  Table [acm].[Principal]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [acm].[Principal](
	[Id] [bigint] IDENTITY(101,1) NOT NULL,
	[Name] [sysname] NOT NULL,
	[Registered] [datetime] NOT NULL,
	[Host] [nvarchar](255) NOT NULL,
	[Application] [nvarchar](255) NOT NULL,
	[Operator] [int] NULL,
 CONSTRAINT [Access_PK] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [Access_AK] UNIQUE CLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [acrawcache].[Offer]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [acrawcache].[Offer](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Request] [bigint] NOT NULL,
	[AmountRub] [money] NULL,
	[GdsId] [smallint] NULL,
	[Vendor] [char](2) NULL,
	[RouteStart] [datetime] NULL,
	[Routes] [nvarchar](max) NULL,
 CONSTRAINT [Offer_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [acrawcache].[Request]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [acrawcache].[Request](
	[SearchKey] [varchar](128) NOT NULL,
	[Submission] [bigint] NOT NULL,
	[Row] [int] NOT NULL,
	[Response] [nvarchar](max) NULL,
	[Status] [int] NULL,
	[Processed] [datetime] NULL,
	[Expires] [datetime] NULL,
	[TTL] [int] NULL,
	[AltId] [bigint] NULL,
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [Request_PK] PRIMARY KEY CLUSTERED 
(
	[SearchKey] ASC,
	[Submission] ASC,
	[Row] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [acrawcache].[Segment]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [acrawcache].[Segment](
	[RouteStart] [datetime] NOT NULL,
	[Offer] [bigint] NOT NULL,
	[RouteOrdinal] [tinyint] NOT NULL,
	[Ordinal] [tinyint] NOT NULL,
	[DepartureTime] [datetime] NULL,
	[Baggage] [varchar](40) NULL,
	[DepartureCountry] [char](2) NULL,
	[DepartureCity] [char](3) NULL,
	[DepartureAirport] [char](3) NULL,
	[ArrivalCountry] [char](2) NULL,
	[ArrivalCity] [char](3) NULL,
	[ArrivalAirport] [char](3) NULL,
	[ArrivalTime] [datetime] NULL,
	[VendorCode] [char](2) NULL,
	[Vendor] [nvarchar](100) NULL,
	[Flight] [varchar](40) NULL,
	[FareCode] [varchar](40) NULL,
	[CarrierCode] [char](2) NULL,
	[Seats] [tinyint] NULL,
	[AircraftCode] [varchar](40) NULL,
	[Aircraft] [nvarchar](100) NULL,
	[ServiceClassType] [varchar](20) NULL,
	[ServiceClass] [char](2) NULL,
 CONSTRAINT [Segment_PK] PRIMARY KEY CLUSTERED 
(
	[Offer] ASC,
	[RouteStart] ASC,
	[RouteOrdinal] ASC,
	[Ordinal] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[ActivePropertyBusinessModel]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[ActivePropertyBusinessModel](
	[EANHotelId] [int] NOT NULL,
	[SequenceNumber] [int] NULL,
	[Name] [varchar](70) NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[StateProvince] [varchar](2) NULL,
	[PostalCode] [varchar](15) NULL,
	[Country] [varchar](2) NULL,
	[Latitude] [numeric](8, 5) NULL,
	[Longitude] [numeric](8, 5) NULL,
	[AirportCode] [varchar](3) NULL,
	[PropertyCategory] [int] NULL,
	[PropertyCurrency] [varchar](3) NULL,
	[StarRating] [numeric](2, 1) NULL,
	[Confidence] [int] NULL,
	[SupplierType] [varchar](3) NULL,
	[Location] [varchar](80) NULL,
	[ChainCodeID] [int] NULL,
	[RegionId] [int] NULL,
	[HighRate] [numeric](19, 4) NULL,
	[LowRate] [numeric](19, 4) NULL,
	[CheckInTime] [varchar](10) NULL,
	[CheckOutTime] [varchar](10) NULL,
	[BusinessModelMask] [int] NULL,
	[Updated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[EANHotelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[ActivePropertyList]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[ActivePropertyList](
	[EANHotelId] [int] NOT NULL,
	[SequenceNumber] [int] NULL,
	[Name] [varchar](70) NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[StateProvince] [varchar](2) NULL,
	[PostalCode] [varchar](15) NULL,
	[Country] [varchar](2) NULL,
	[Latitude] [numeric](8, 5) NULL,
	[Longitude] [numeric](8, 5) NULL,
	[AirportCode] [varchar](3) NULL,
	[PropertyCategory] [int] NULL,
	[PropertyCurrency] [varchar](3) NULL,
	[StarRating] [numeric](2, 1) NULL,
	[Confidence] [int] NULL,
	[SupplierType] [varchar](3) NULL,
	[Location] [varchar](80) NULL,
	[ChainCodeID] [int] NULL,
	[RegionId] [int] NULL,
	[HighRate] [numeric](19, 4) NULL,
	[LowRate] [numeric](19, 4) NULL,
	[CheckInTime] [varchar](10) NULL,
	[CheckOutTime] [varchar](10) NULL,
	[Updated] [datetime] NULL,
 CONSTRAINT [ActivePropertyList_PK] PRIMARY KEY CLUSTERED 
(
	[EANHotelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[AirportCoordinatesList]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[AirportCoordinatesList](
	[AirportId] [int] NOT NULL,
	[AirportCode] [varchar](3) NOT NULL,
	[AirportName] [varchar](70) NULL,
	[Latitude] [numeric](9, 6) NULL,
	[Longitude] [numeric](9, 6) NULL,
	[MainCityID] [int] NULL,
	[CountryCode] [varchar](2) NULL,
	[Updated] [datetime] NULL,
 CONSTRAINT [AirportCoordinatesList_PK] PRIMARY KEY CLUSTERED 
(
	[AirportCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[AliasRegionList]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[AliasRegionList](
	[RegionId] [int] NOT NULL,
	[LanguageCode] [varchar](5) NULL,
	[AliasString] [varchar](255) NULL,
	[Updated] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[AreaAttractionsList]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[AreaAttractionsList](
	[EANHotelId] [int] NOT NULL,
	[LanguageCode] [varchar](5) NULL,
	[AreaAttractions] [varchar](max) NULL,
	[Updated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[EANHotelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[AttributeList]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[AttributeList](
	[AttributeID] [int] NOT NULL,
	[LanguageCode] [varchar](5) NULL,
	[AttributeDesc] [varchar](255) NULL,
	[Type] [varchar](15) NULL,
	[SubType] [varchar](15) NULL,
	[Updated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[AttributeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[ChainList]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[ChainList](
	[ChainCodeID] [int] NOT NULL,
	[ChainName] [varchar](30) NULL,
	[Updated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ChainCodeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[CityCoordinatesList]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[CityCoordinatesList](
	[RegionId] [int] NOT NULL,
	[RegionName] [varchar](255) NULL,
	[Coordinates] [varchar](max) NULL,
	[Updated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[RegionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[CountryList]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[CountryList](
	[CountryID] [int] NOT NULL,
	[LanguageCode] [varchar](5) NULL,
	[CountryName] [varchar](250) NULL,
	[CountryCode] [varchar](2) NOT NULL,
	[Transliteration] [varchar](256) NULL,
	[ContinentID] [int] NULL,
	[Updated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CountryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[DiningDescriptionList]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[DiningDescriptionList](
	[EANHotelId] [int] NOT NULL,
	[LanguageCode] [varchar](5) NULL,
	[DiningDescription] [varchar](max) NULL,
	[Updated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[EANHotelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[HotelImageList]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[HotelImageList](
	[EANHotelId] [int] NOT NULL,
	[Caption] [varchar](70) NULL,
	[URL] [varchar](150) NOT NULL,
	[Width] [int] NULL,
	[Height] [int] NULL,
	[ByteSize] [int] NULL,
	[ThumbnailURL] [varchar](300) NULL,
	[DefaultImage] [bit] NULL,
	[Updated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[URL] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[NeighborhoodCoordinatesList]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[NeighborhoodCoordinatesList](
	[RegionId] [int] NOT NULL,
	[RegionName] [varchar](255) NULL,
	[Coordinates] [varchar](max) NULL,
	[Updated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[RegionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[ParentRegionList]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[ParentRegionList](
	[RegionId] [int] NOT NULL,
	[RegionType] [varchar](50) NULL,
	[RelativeSignificance] [varchar](3) NULL,
	[SubClass] [varchar](50) NULL,
	[RegionName] [varchar](255) NULL,
	[RegionNameLong] [varchar](510) NULL,
	[ParentRegionId] [int] NULL,
	[ParentRegionType] [varchar](50) NULL,
	[ParentRegionName] [varchar](255) NULL,
	[ParentRegionNameLong] [varchar](510) NULL,
	[Updated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[RegionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[PointsOfInterestCoordinatesList]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[PointsOfInterestCoordinatesList](
	[RegionId] [int] NOT NULL,
	[RegionName] [varchar](255) NULL,
	[RegionNameLong] [varchar](191) NOT NULL,
	[Latitude] [numeric](9, 6) NULL,
	[Longitude] [numeric](9, 6) NULL,
	[SubClassification] [varchar](20) NULL,
	[Updated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[RegionNameLong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[PolicyDescriptionList]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[PolicyDescriptionList](
	[EANHotelId] [int] NOT NULL,
	[LanguageCode] [varchar](5) NULL,
	[PolicyDescription] [varchar](max) NULL,
	[Updated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[EANHotelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[PropertyAmenitiesList]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[PropertyAmenitiesList](
	[EANHotelId] [int] NOT NULL,
	[LanguageCode] [varchar](5) NULL,
	[PropertyAmenitiesDescription] [varchar](max) NULL,
	[Updated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[EANHotelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[PropertyAttributeLink]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[PropertyAttributeLink](
	[EANHotelId] [int] NOT NULL,
	[AttributeID] [int] NOT NULL,
	[LanguageCode] [varchar](5) NULL,
	[AppendTxt] [varchar](191) NULL,
	[Updated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[EANHotelId] ASC,
	[AttributeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[PropertyBusinessAmenitiesList]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[PropertyBusinessAmenitiesList](
	[EANHotelId] [int] NOT NULL,
	[LanguageCode] [varchar](5) NULL,
	[PropertyBusinessAmenitiesDescription] [varchar](max) NULL,
	[Updated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[EANHotelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[PropertyDescriptionList]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[PropertyDescriptionList](
	[EANHotelId] [int] NOT NULL,
	[LanguageCode] [varchar](5) NULL,
	[PropertyDescription] [varchar](max) NULL,
	[Updated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[EANHotelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[PropertyFeesList]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[PropertyFeesList](
	[EANHotelId] [int] NOT NULL,
	[LanguageCode] [varchar](5) NULL,
	[PropertyFeesDescription] [varchar](max) NULL,
	[Updated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[EANHotelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[PropertyLocationList]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[PropertyLocationList](
	[EANHotelId] [int] NOT NULL,
	[LanguageCode] [varchar](5) NULL,
	[PropertyLocationDescription] [varchar](max) NULL,
	[Updated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[EANHotelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[PropertyMandatoryFeesList]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[PropertyMandatoryFeesList](
	[EANHotelId] [int] NOT NULL,
	[LanguageCode] [varchar](5) NULL,
	[PropertyMandatoryFeesDescription] [varchar](max) NULL,
	[Updated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[EANHotelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[PropertynationalratingsList]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[PropertynationalratingsList](
	[EANHotelId] [int] NOT NULL,
	[LanguageCode] [varchar](5) NULL,
	[PropertyNationalRatingsDescription] [varchar](max) NULL,
	[Updated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[EANHotelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[PropertyRenovationsList]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[PropertyRenovationsList](
	[EANHotelId] [int] NOT NULL,
	[LanguageCode] [varchar](5) NULL,
	[PropertyRenovationsDescription] [varchar](max) NULL,
	[Updated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[EANHotelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[PropertyroomsList]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[PropertyroomsList](
	[EANHotelId] [int] NOT NULL,
	[LanguageCode] [varchar](5) NULL,
	[PropertyRoomsDescription] [varchar](max) NULL,
	[Updated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[EANHotelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[PropertyTypeList]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[PropertyTypeList](
	[PropertyCategory] [int] NOT NULL,
	[LanguageCode] [varchar](5) NULL,
	[PropertyCategoryDesc] [varchar](256) NULL,
	[Updated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[PropertyCategory] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[RecreationDescriptionList]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[RecreationDescriptionList](
	[EANHotelId] [int] NOT NULL,
	[LanguageCode] [varchar](5) NULL,
	[RecreationDescription] [varchar](max) NULL,
	[Updated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[EANHotelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[RegionCentercoordinatesList]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[RegionCentercoordinatesList](
	[RegionId] [int] NOT NULL,
	[RegionName] [varchar](255) NULL,
	[CenterLatitude] [numeric](9, 6) NULL,
	[CenterLongitude] [numeric](9, 6) NULL,
	[Updated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[RegionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[RegionEANHotelIdMapping]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[RegionEANHotelIdMapping](
	[RegionId] [int] NOT NULL,
	[EANHotelId] [int] NOT NULL,
	[Updated] [datetime] NULL,
 CONSTRAINT [RegionEANHotelIdMapping_PK] PRIMARY KEY CLUSTERED 
(
	[RegionId] ASC,
	[EANHotelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[RoomTypeList]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[RoomTypeList](
	[EANHotelId] [int] NOT NULL,
	[RoomTypeID] [int] NOT NULL,
	[LanguageCode] [varchar](5) NULL,
	[RoomTypeImage] [varchar](256) NULL,
	[RoomTypeName] [varchar](200) NULL,
	[RoomTypeDescription] [varchar](max) NULL,
	[Updated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[EANHotelId] ASC,
	[RoomTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[SpaDescriptionList]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[SpaDescriptionList](
	[EANHotelId] [int] NOT NULL,
	[LanguageCode] [varchar](5) NULL,
	[SpaDescription] [varchar](max) NULL,
	[Updated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[EANHotelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [eanraw].[WhatToExpectList]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eanraw].[WhatToExpectList](
	[EANHotelId] [int] NOT NULL,
	[LanguageCode] [varchar](5) NULL,
	[WhatToExpect] [varchar](max) NULL,
	[Updated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[EANHotelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [etl].[Submission]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [etl].[Submission](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Action] [bigint] NOT NULL,
	[SourceFolder] [dbo].[URL] NULL,
	[FileName] [dbo].[URL] NULL,
	[FileSize] [int] NULL,
	[InjectorCaption] [dbo].[TxCaption] NOT NULL,
	[Species] [dbo].[TxCaption] NOT NULL,
	[Pertains] [datetime] NULL,
	[Status] [int] NULL,
	[Superior] [bigint] NULL,
	[RowLo] [bigint] NULL,
	[RowHi] [bigint] NULL,
 CONSTRAINT [Submission_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [etl].[SubmissionTransition]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [etl].[SubmissionTransition](
	[Submission] [bigint] NOT NULL,
	[ActionWas] [bigint] NOT NULL,
	[ActionIs] [bigint] NULL,
	[StatusWas] [int] NULL,
	[StatusIs] [int] NULL,
 CONSTRAINT [SubmissionTransition_PK] PRIMARY KEY CLUSTERED 
(
	[Submission] ASC,
	[ActionWas] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [geo].[Locus]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [geo].[Locus](
	[Id] [bigint] NOT NULL,
	[Caption] [nvarchar](100) NOT NULL,
	[Kind] [int] NOT NULL,
	[GmlKind] [varchar](20) NOT NULL,
	[Longitude] [float] NULL,
	[Latitude] [float] NULL,
	[Shape] [geography] NULL,
	[Country] [dbo].[IsoCountry] NULL,
	[TimeZoneOffset] [smallint] NULL,
	[Superior] [bigint] NULL,
	[PathId] [hierarchyid] NULL,
	[Level] [int] NULL,
 CONSTRAINT [Locus_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [Locus_AK1] UNIQUE NONCLUSTERED 
(
	[Caption] ASC,
	[Superior] ASC,
	[Kind] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [geo].[LocusAlias]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [geo].[LocusAlias](
	[Locus] [bigint] NOT NULL,
	[Locale] [tinyint] NOT NULL,
	[Caption] [nvarchar](100) NOT NULL,
	[Alias] [nvarchar](100) NOT NULL,
 CONSTRAINT [LocusAlias_PK] PRIMARY KEY CLUSTERED 
(
	[Locus] ASC,
	[Locale] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [iata].[Port]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [iata].[Port](
	[Code] [dbo].[IataCode] NOT NULL,
	[Caption] [nvarchar](100) NOT NULL,
	[Country] [dbo].[IsoCountry] NOT NULL,
	[CityCode] [dbo].[IataCode] NULL,
	[Locus] [bigint] NULL,
	[CityLocus] [bigint] NOT NULL,
	[Longitude] [float] NULL,
	[Latitude] [float] NULL,
	[TimeZoneOffset] [smallint] NULL,
 CONSTRAINT [Port_PK] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [ssm].[Action]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ssm].[Action](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Context] [sysname] NOT NULL,
	[DbUser] [sysname] NOT NULL,
	[Impersonation] [sysname] NOT NULL,
	[SystemUser] [sysname] NOT NULL,
	[Host] [sysname] NOT NULL,
	[App] [sysname] NOT NULL,
	[Occured] [datetime2](7) NOT NULL,
	[Details] [xml] NULL,
 CONSTRAINT [Action_PK] PRIMARY KEY CLUSTERED 
(
	[Occured] DESC,
	[Impersonation] ASC,
	[Context] ASC,
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [ssm].[DDL_Issue]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ssm].[DDL_Issue](
	[Posted] [datetime] NOT NULL,
	[Event] [sysname] NOT NULL,
	[Target] [sysname] NULL,
	[Commands] [nvarchar](max) NOT NULL,
	[Details] [xml] NOT NULL,
	[DbUser] [sysname] NOT NULL,
	[Impersonation] [sysname] NOT NULL,
	[SystemUser] [sysname] NOT NULL,
	[Row] [int] IDENTITY(1,1) NOT NULL,
	[Host] [varchar](64) NULL,
	[Application] [nvarchar](255) NULL,
 CONSTRAINT [DDL_Issue_PK] PRIMARY KEY NONCLUSTERED 
(
	[Row] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Index [DbChange_IE_Posted]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE CLUSTERED INDEX [DbChange_IE_Posted] ON [ssm].[DDL_Issue]
(
	[Posted] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [ssm].[Error]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ssm].[Error](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Raised] [datetime] NOT NULL,
	[Principal] [sysname] NOT NULL,
	[Number] [int] NOT NULL,
	[Severity] [int] NULL,
	[State] [int] NULL,
	[Context] [nvarchar](126) NULL,
	[ErrorLine] [int] NULL,
	[ErrorMessage] [nvarchar](4000) NULL,
	[Login] [sysname] NULL,
 CONSTRAINT [Error_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [ssm].[TablesToAudit]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ssm].[TablesToAudit](
	[SchemaName] [sysname] NOT NULL,
	[TableName] [sysname] NOT NULL,
	[IndexName] [sysname] NULL,
	[ObjectId] [int] NULL,
 CONSTRAINT [TablesToAudit_PK] PRIMARY KEY CLUSTERED 
(
	[SchemaName] ASC,
	[TableName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  UserDefinedFunction [dev].[iObject]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- select * from dev.iObject(default, default)
-- select * from dev.iObject(default, 'Party')
create function [dev].[iObject](@SchemaName sysname = '%', @ObjectName sysname = '%')
returns table as return
select
  o.[object_id],
  o.name,
  o.schema_id,
  o.[type],
  s.name schema_name,
  s.name + '.' + o.[name] fullname,
  o.parent_object_id,
  s.principal_id,
  o.create_date,
  o.modify_date,
  o.is_ms_shipped
from sys.all_objects o
join sys.schemas s
  on s.schema_id = o.schema_id
where s.name like @SchemaName
  and o.name like @ObjectName;

GO
/****** Object:  UserDefinedFunction [dev].[iTable]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- select * from dev.iTable('cdm', default)
-- select * from dev.iTable('dbo', default)
create function [dev].[iTable](@SchemaName sysname = '%', @ObjectName sysname = '%')
returns table as return
select
  o.[object_id],
  o.name,
  o.[schema_id],
  o.[schema_name],
  o.fullname,
  o.principal_id,
  o.create_date,
  o.modify_date,
  o.is_ms_shipped
from dev.iObject(@SchemaName, @ObjectName) o
where o.[type] = 'U';

GO
/****** Object:  UserDefinedFunction [dev].[iColumn]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- select * from dev.iColumn(OBJECT_ID(N'[etl].[SubmissionV0]')) c
CREATE FUNCTION [dev].[iColumn](@TableId int = Null)
RETURNS TABLE AS RETURN
SELECT
  o.object_id TableId
, OBJECT_SCHEMA_NAME(o.object_id, DB_ID()) TableSchemaName
, o.name as TableName
, o.create_date TableCreated
, o.modify_date TableModified
, o.type_desc ObjectXtype
, o.parent_object_id Parent
, c.column_id Ordinal
, c.name ColumnName
, st.name BaseTypeName
, CASE WHEN ut.name <> st.name THEN ut.name END UserTypeName
, IsNull(st.name, '?')
+ CASE
    WHEN c.system_type_id in (106,62) THEN '(' + REPLACE(CONVERT(varchar(10), c.[precision]),'-1','max') + ',' + CONVERT(varchar(10), c.scale) + ')'
    WHEN c.system_type_id in (42) THEN '(' + CONVERT(varchar(10), c.scale) + ')'
    WHEN c.system_type_id in (165,167,175,231,239) THEN '(' + REPLACE(CONVERT(varchar(10), c.max_length), '-1','max') + ')'
    ELSE ''
  END AS TypeDesignator
, c.is_identity as IsIdentity
, c.is_nullable as IsNullable
, c.is_computed as IsComputed
, c.collation_name Collation
, c.system_type_id
, c.user_type_id
, c.[precision] [Precision]
, c.scale Scale
, c.max_length
, st.prec PrecisionLimit
--, t.xtype
--, tt.scale ScaleLimit
--, tt.[length] SizeLimit
FROM sys.objects o
JOIN sys.columns c
  ON o.[object_id] = c.[object_id]
-- AND o.xtype	in ('u', 'v', 'pc', 'tt', 's', 'it', 'fn', 'p', 'fs', 'tf', 'if')
LEFT JOIN sys.systypes ut
  ON ut.xusertype = c.user_type_id
LEFT JOIN sys.systypes st
  ON st.xusertype = c.system_type_id
WHERE (o.[object_id] = @TableId OR @TableId Is Null);

GO
/****** Object:  UserDefinedFunction [uti].[iTally1000]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- SET STATISTICS TIME ON;
-- SELECT * FROM uti.iTally1000(0, Null) --ORDER BY X
-- SELECT * FROM uti.iTally1000(-30, 99) --ORDER BY X
-- SELECT * FROM uti.iTally1000(0, 999) --ORDER BY X
CREATE FUNCTION [uti].[iTally1000](@Start int, @Stop int)
RETURNS TABLE AS
RETURN
-- WITH Z0(X) AS (SELECT N.X FROM (VALUES (0), (1), (2), (3), (4), (5), (6), (7), (8), (9)) AS N(X));
WITH Z0(X) AS
(
  SELECT 0 X
  UNION ALL
  SELECT X+1 X FROM Z0 WHERE X < 9
)
,Z00(X) AS
(
  SELECT C.X*10 + B.X*100 + A.X + @Start
  FROM Z0 A, Z0 B, Z0 C
)
SELECT X
FROM Z00
WHERE (X <= @Stop OR @Stop Is Null)
--ORDER BY X
--OPTION (MAXRECURSION 9)
;

GO
/****** Object:  UserDefinedFunction [uti].[iTally367]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- SET STATISTICS TIME ON;
-- SELECT * FROM uti.iTally367('20160730', '20160805')
-- SELECT * FROM uti.iTally367('20160730', Null)
CREATE FUNCTION [uti].[iTally367](@Start date, @Stop date)
RETURNS TABLE AS
RETURN
WITH Z0(X) AS
(
  SELECT TOP 367
    ROW_NUMBER() OVER (ORDER BY object_id, column_id) as X
  FROM sys.columns
)
SELECT DATEADD(day, X - 1, @Start) as X
FROM Z0
WHERE (DATEADD(day, X - 1, @Start) <= @Stop OR @Stop Is Null)
;
GO
/****** Object:  View [acrawcache].[RequestInV]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [acrawcache].[RequestInV]
AS
SELECT
  r.Submission
, r.SearchKey
, r.[Row]
, r.Response
FROM [acrawcache].[Request] r;

GO
/****** Object:  View [etl].[SubmissionV0]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [etl].[SubmissionV0]
AS
SELECT
	s.Id
, s.[Action]
,	a.Occured Submitted
,	s.SourceFolder
,	s.[FileName]
, s.FileSize
,	s.InjectorCaption
, s.Species
,	a.Impersonation
, a.DbUser
, a.SystemUser
, a.Host
, a.Context
, a.App
, s.Pertains
,	s.[Status]
, s.Superior
, s.RowLo
, s.RowHi
FROM etl.Submission s
JOIN ssm.[Action] a
   ON s.[Action] = a.Id;

GO
/****** Object:  Index [Offer_FK_Request]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE NONCLUSTERED INDEX [Offer_FK_Request] ON [acrawcache].[Offer]
(
	[Request] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [Request_AK1]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [Request_AK1] ON [acrawcache].[Request]
(
	[Id] ASC
)
INCLUDE ( 	[SearchKey],
	[Submission],
	[Row]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [Request_FK_Submission]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE NONCLUSTERED INDEX [Request_FK_Submission] ON [acrawcache].[Request]
(
	[Submission] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ActivePropertyBusinessModel_IE_GeoLoc]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE NONCLUSTERED INDEX [ActivePropertyBusinessModel_IE_GeoLoc] ON [eanraw].[ActivePropertyBusinessModel]
(
	[Latitude] ASC,
	[Longitude] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ActivePropertyBusinessModel_IE_RegionId]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE NONCLUSTERED INDEX [ActivePropertyBusinessModel_IE_RegionId] ON [eanraw].[ActivePropertyBusinessModel]
(
	[RegionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ActivePropertyList_IE_GeoLoc]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE NONCLUSTERED INDEX [ActivePropertyList_IE_GeoLoc] ON [eanraw].[ActivePropertyList]
(
	[Latitude] ASC,
	[Longitude] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ActivePropertyList_IE_RegionId]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE NONCLUSTERED INDEX [ActivePropertyList_IE_RegionId] ON [eanraw].[ActivePropertyList]
(
	[RegionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AirportCoordinatesList_IE_AirportName]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE NONCLUSTERED INDEX [AirportCoordinatesList_IE_AirportName] ON [eanraw].[AirportCoordinatesList]
(
	[AirportName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [AirportCoordinatesList_IE_GeoLoc]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE NONCLUSTERED INDEX [AirportCoordinatesList_IE_GeoLoc] ON [eanraw].[AirportCoordinatesList]
(
	[Latitude] ASC,
	[Longitude] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [AirportCoordinatesList_IE_MainCityID]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE NONCLUSTERED INDEX [AirportCoordinatesList_IE_MainCityID] ON [eanraw].[AirportCoordinatesList]
(
	[MainCityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [AliasRegionList_IE_RegionId]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE NONCLUSTERED INDEX [AliasRegionList_IE_RegionId] ON [eanraw].[AliasRegionList]
(
	[RegionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CountryList_AK_CountryCode]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [CountryList_AK_CountryCode] ON [eanraw].[CountryList]
(
	[CountryCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CountryList_AK_CountryName]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [CountryList_AK_CountryName] ON [eanraw].[CountryList]
(
	[CountryName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [HotelImageList_IE_EANHotelId]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE NONCLUSTERED INDEX [HotelImageList_IE_EANHotelId] ON [eanraw].[HotelImageList]
(
	[EANHotelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ParentRegionList_IE_Parent]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE NONCLUSTERED INDEX [ParentRegionList_IE_Parent] ON [eanraw].[ParentRegionList]
(
	[ParentRegionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [PointsOfInterestCoordinatesList_IE_GeoLoc]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE NONCLUSTERED INDEX [PointsOfInterestCoordinatesList_IE_GeoLoc] ON [eanraw].[PointsOfInterestCoordinatesList]
(
	[Latitude] ASC,
	[Longitude] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [PointsOfInterestCoordinatesList_IE_RegionId]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE NONCLUSTERED INDEX [PointsOfInterestCoordinatesList_IE_RegionId] ON [eanraw].[PointsOfInterestCoordinatesList]
(
	[RegionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [PropertyAttributeLink_AK1]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [PropertyAttributeLink_AK1] ON [eanraw].[PropertyAttributeLink]
(
	[AttributeID] ASC,
	[EANHotelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [RegionEANHotelIdMapping_AK]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [RegionEANHotelIdMapping_AK] ON [eanraw].[RegionEANHotelIdMapping]
(
	[EANHotelId] ASC,
	[RegionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [Locus_AK0]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [Locus_AK0] ON [geo].[Locus]
(
	[Level] ASC,
	[PathId] ASC
)
INCLUDE ( 	[Id],
	[Caption],
	[Superior],
	[Kind]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [LocusAlias_IE_Alias]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE NONCLUSTERED INDEX [LocusAlias_IE_Alias] ON [geo].[LocusAlias]
(
	[Alias] ASC
)
INCLUDE ( 	[Locus],
	[Caption],
	[Locale]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [Port_IE_CityCode]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE NONCLUSTERED INDEX [Port_IE_CityCode] ON [iata].[Port]
(
	[CityCode] ASC
)
INCLUDE ( 	[Code],
	[Locus],
	[CityLocus]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [Action_AK0]    Script Date: 11/22/2016 1:59:48 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [Action_AK0] ON [ssm].[Action]
(
	[Id] ASC
)
INCLUDE ( 	[Occured],
	[Impersonation],
	[Context]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [acm].[Principal] ADD  DEFAULT (suser_sname()) FOR [Name]
GO
ALTER TABLE [acm].[Principal] ADD  DEFAULT (getdate()) FOR [Registered]
GO
ALTER TABLE [acm].[Principal] ADD  DEFAULT (host_name()) FOR [Host]
GO
ALTER TABLE [acm].[Principal] ADD  DEFAULT (program_name()) FOR [Application]
GO
ALTER TABLE [acrawcache].[Request] ADD  DEFAULT ((-1)) FOR [Row]
GO
ALTER TABLE [eanraw].[ActivePropertyBusinessModel] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[ActivePropertyList] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[AirportCoordinatesList] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[AliasRegionList] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[AreaAttractionsList] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[AttributeList] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[ChainList] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[CityCoordinatesList] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[CountryList] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[DiningDescriptionList] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[HotelImageList] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[NeighborhoodCoordinatesList] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[ParentRegionList] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[PointsOfInterestCoordinatesList] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[PolicyDescriptionList] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[PropertyAmenitiesList] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[PropertyAttributeLink] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[PropertyBusinessAmenitiesList] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[PropertyDescriptionList] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[PropertyFeesList] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[PropertyLocationList] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[PropertyMandatoryFeesList] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[PropertynationalratingsList] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[PropertyRenovationsList] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[PropertyroomsList] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[PropertyTypeList] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[RecreationDescriptionList] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[RegionCentercoordinatesList] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[RegionEANHotelIdMapping] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[RoomTypeList] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[SpaDescriptionList] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [eanraw].[WhatToExpectList] ADD  DEFAULT (getdate()) FOR [Updated]
GO
ALTER TABLE [ssm].[Action] ADD  DEFAULT (user_name()) FOR [DbUser]
GO
ALTER TABLE [ssm].[Action] ADD  DEFAULT (suser_sname()) FOR [Impersonation]
GO
ALTER TABLE [ssm].[Action] ADD  DEFAULT (original_login()) FOR [SystemUser]
GO
ALTER TABLE [ssm].[Action] ADD  DEFAULT (host_name()) FOR [Host]
GO
ALTER TABLE [ssm].[Action] ADD  DEFAULT (app_name()) FOR [App]
GO
ALTER TABLE [ssm].[Action] ADD  DEFAULT (sysdatetime()) FOR [Occured]
GO
ALTER TABLE [ssm].[Error] ADD  CONSTRAINT [Error_DF_Raised]  DEFAULT (getdate()) FOR [Raised]
GO
ALTER TABLE [acrawcache].[Offer]  WITH CHECK ADD  CONSTRAINT [Offer_FK_Request] FOREIGN KEY([Request])
REFERENCES [acrawcache].[Request] ([Id])
GO
ALTER TABLE [acrawcache].[Offer] CHECK CONSTRAINT [Offer_FK_Request]
GO
ALTER TABLE [acrawcache].[Request]  WITH CHECK ADD  CONSTRAINT [Request_FK_Submission] FOREIGN KEY([Submission])
REFERENCES [etl].[Submission] ([Id])
GO
ALTER TABLE [acrawcache].[Request] CHECK CONSTRAINT [Request_FK_Submission]
GO
ALTER TABLE [etl].[Submission]  WITH CHECK ADD  CONSTRAINT [Submission_FK_Action] FOREIGN KEY([Action])
REFERENCES [ssm].[Action] ([Id])
GO
ALTER TABLE [etl].[Submission] CHECK CONSTRAINT [Submission_FK_Action]
GO
ALTER TABLE [etl].[Submission]  WITH CHECK ADD  CONSTRAINT [Submission_FK_Superior] FOREIGN KEY([Superior])
REFERENCES [etl].[Submission] ([Id])
GO
ALTER TABLE [etl].[Submission] CHECK CONSTRAINT [Submission_FK_Superior]
GO
ALTER TABLE [geo].[Locus]  WITH CHECK ADD  CONSTRAINT [Locus_FK_Superior] FOREIGN KEY([Superior])
REFERENCES [geo].[Locus] ([Id])
GO
ALTER TABLE [geo].[Locus] CHECK CONSTRAINT [Locus_FK_Superior]
GO
ALTER TABLE [geo].[LocusAlias]  WITH CHECK ADD  CONSTRAINT [LocusAlias_FK_Locus] FOREIGN KEY([Locus])
REFERENCES [geo].[Locus] ([Id])
GO
ALTER TABLE [geo].[LocusAlias] CHECK CONSTRAINT [LocusAlias_FK_Locus]
GO
ALTER TABLE [iata].[Port]  WITH CHECK ADD  CONSTRAINT [Port_FK_CityLocus] FOREIGN KEY([CityLocus])
REFERENCES [geo].[Locus] ([Id])
GO
ALTER TABLE [iata].[Port] CHECK CONSTRAINT [Port_FK_CityLocus]
GO
ALTER TABLE [iata].[Port]  WITH CHECK ADD  CONSTRAINT [Port_FK_Locus] FOREIGN KEY([Locus])
REFERENCES [geo].[Locus] ([Id])
GO
ALTER TABLE [iata].[Port] CHECK CONSTRAINT [Port_FK_Locus]
GO
/****** Object:  StoredProcedure [dev].[BackupMe]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- EXEC [dev].[BackupMe]
CREATE PROCEDURE [dev].[BackupMe]
AS
DECLARE @Me sysname = DB_NAME()
EXEC master.dbo.BackupDb @Me, 1

GO
/****** Object:  StoredProcedure [dev].[CreateSchema]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dev].[CreateSchema]
  @SchemaName sysname,
  @Explanation nvarchar(1000) = Null
AS
IF @SchemaName Is Null
  RETURN -1;

IF SCHEMA_ID(@SchemaName) Is Null
  EXECUTE(N'CREATE SCHEMA ' + @SchemaName + ' AUTHORIZATION [dbo]');

IF @@ERROR <> 0 OR SCHEMA_ID(@SchemaName) Is Null
  RETURN -2;

EXEC dev.ExplainSchema @SchemaName, @Explanation, default;

RETURN @@ERROR;

GO
/****** Object:  StoredProcedure [dev].[DropAllSchemata]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dev].[DropAllSchemata]
AS
SET NOCOUNT ON;

DECLARE @Schemas TABLE(Name sysname);

INSERT @Schemas(Name)
SELECT s.[name]
FROM sys.schemas s
WHERE s.schema_id <> s.principal_id;

DECLARE @aSchema sysname;

WHILE 1=1 BEGIN
  SELECT TOP 1 @aSchema = Name FROM @Schemas;
  IF @@ROWCOUNT = 0 BREAK;
  EXEC('DROP SCHEMA ' + @aSchema);
  DELETE @Schemas WHERE Name = @aSchema;
END

RETURN @@ERROR;

GO
/****** Object:  StoredProcedure [dev].[ExplainColumn]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dev].[ExplainColumn]
  @SchemaTableColumnName sysname
, @Value nvarchar(1000)
, @Tag sysname = N'MS_Description'
AS
DECLARE @X int;

DECLARE
  @SchemaName sysname = PARSENAME(@SchemaTableColumnName, 3)
, @TableName sysname = PARSENAME(@SchemaTableColumnName, 2)
, @ColumnName sysname = PARSENAME(@SchemaTableColumnName, 1)
;

DECLARE
  @Schema int = SCHEMA_ID(@SchemaName)
, @Table int
, @Column int
, @ObjectType sysname
;

SELECT
  @Table = TBL.object_id
 ,@ObjectType = OT.Caption
FROM
(VALUES
  ('U', 'TABLE')
 ,('V', 'VIEW')
 --,('IF', 'FUNCTION')
) AS OT(Code, Caption)
JOIN sys.objects TBL -- sys.tables
   ON TBL.name = @TableName
  AND TBL.schema_id = @Schema
  AND TBL.[type] = OT.Code;
IF @@ROWCOUNT = 0 BEGIN
  RAISERROR('Table [%s] not found!', 11, 1, @TableName);
  RETURN @@ERROR;
END

SELECT
  @Column = COL.column_id
FROM sys.columns COL
WHERE COL.name = @ColumnName
  AND COL.object_id = @Table;
IF @@ROWCOUNT = 0 BEGIN
  RAISERROR('Column [%s] of the table [%s] not found!', 11, 2, @ColumnName, @TableName);
  RETURN @@ERROR;
END

/*
SELECT *
FROM sys.schemas SCH
JOIN sys.tables TBL
  ON SCH.schema_id = TBL.schema_id 
JOIN sys.columns COL
  ON TBL.object_id = COL.object_id
ORDER BY SCH.name, TBL.name, COL.column_id
*/
IF EXISTS
(
  SELECT *
  FROM fn_listextendedproperty (@Tag, N'SCHEMA', @SchemaName, @ObjectType, @TableName, N'COLUMN', @ColumnName)
)
GOTO Change;

EXEC @X = sys.sp_addextendedproperty
  @level0type = N'SCHEMA'
, @level0name = @SchemaName
, @level1type = @ObjectType
, @level1name = @TableName
, @level2type = N'COLUMN'
, @level2name = @ColumnName
, @name = @Tag 
, @value = @Value;

RETURN @X;

Change:

EXEC @X = sys.sp_updateextendedproperty
  @level0type = N'SCHEMA'
, @level0name = @SchemaName
, @level1type = @ObjectType
, @level1name = @TableName
, @level2type = N'COLUMN'
, @level2name = @ColumnName
, @name = @Tag 
, @value = @Value;

RETURN @X;

GO
/****** Object:  StoredProcedure [dev].[ExplainSchema]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dev].[ExplainSchema]
  @SchemaName sysname
, @Explanation nvarchar(1000)
, @Tag sysname = N'MS_Description'
AS
DECLARE @X int;

IF SCHEMA_ID(@SchemaName) Is Null BEGIN
  IF @Explanation IS NOT Null
    RAISERROR('Schema [%s] not found!', 11, 1, @SchemaName);
  RETURN @@ERROR;
END

IF EXISTS (SELECT * FROM fn_listextendedproperty (@Tag, N'SCHEMA', @SchemaName, null, null, null, null))
  GOTO Change;

EXEC @X = sys.sp_addextendedproperty
  @level0type = N'SCHEMA'
, @level0name = @SchemaName
, @name = @Tag 
, @value = @Explanation;

RETURN @X;

Change:

IF @Explanation Is Null
  EXEC @X = sys.sp_dropextendedproperty
    @level0type = N'SCHEMA'
  , @level0name = @SchemaName
  , @name = @Tag
ELSE
  EXEC @X = sys.sp_updateextendedproperty
    @level0type = N'SCHEMA'
  , @level0name = @SchemaName
  , @name = @Tag 
  , @value = @Explanation;

RETURN @X;

GO
/****** Object:  StoredProcedure [dev].[ExplainTable]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dev].[ExplainTable]
  @SchemaTableName sysname
, @Value nvarchar(1000)
, @Tag sysname = N'MS_Description'
AS
DECLARE @X int;

DECLARE
  @SchemaName sysname = PARSENAME(@SchemaTableName, 2)
, @TableName sysname = PARSENAME(@SchemaTableName, 1)
;

DECLARE
  @Schema int = SCHEMA_ID(@SchemaName)
, @Table int
, @ObjectType sysname
;

SELECT
  @Table = TBL.object_id
 ,@ObjectType = OT.Caption
FROM
(VALUES
  ('U', 'TABLE')
 ,('V', 'VIEW')
 --,('IF', 'FUNCTION')
) AS OT(Code, Caption)
JOIN sys.objects TBL -- sys.tables
   ON TBL.name = @TableName
  AND TBL.schema_id = @Schema
  AND TBL.[type] = OT.Code;
IF @@ROWCOUNT = 0 BEGIN
  RAISERROR('Table [%s] not found!', 11, 1, @TableName);
  RETURN @@ERROR;
END

IF EXISTS
(
  SELECT *
  FROM fn_listextendedproperty (@Tag, N'SCHEMA', @SchemaName, @ObjectType, @TableName, Null, Null)
)
GOTO Change;

EXEC @X = sys.sp_addextendedproperty
  @level0type = N'SCHEMA'
, @level0name = @SchemaName
, @level1type = @ObjectType
, @level1name = @TableName
, @name = @Tag 
, @value = @Value;

RETURN @X;

Change:

EXEC @X = sys.sp_updateextendedproperty
  @level0type = N'SCHEMA'
, @level0name = @SchemaName
, @level1type = @ObjectType
, @level1name = @TableName
, @name = @Tag 
, @value = @Value;

RETURN @X;

GO
/****** Object:  StoredProcedure [dev].[HelpColumns]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- EXEC dev.HelpColumns N'[etl].[SubmissionV0]'
CREATE PROC [dev].[HelpColumns]
  @TableName sysname = Null
AS
DECLARE @Yes bit = 1, @No bit = 0;
SELECT
  C.Ordinal
, C.ColumnName
, IsNull(C.UserTypeName + '/', '') + IsNull(C.TypeDesignator, '') AS Domain
, CASE C.IsNullable
    WHEN 1 THEN @No
    WHEN 0 THEN @Yes
  END IsMandatory
, C.IsComputed
, C.IsIdentity
, X.[value] AS Explanation
--, X.major_id XpMajor_id
--, X.minor_id XpMinor_id
--FROM dev.iColumn(IsNull(OBJECT_ID(@TableName), 0)) C
FROM dev.iColumn(OBJECT_ID(@TableName)) C
LEFT JOIN sys.extended_properties X
   ON X.major_id = C.TableId
  AND X.class = 1
  AND X.minor_id = C.Ordinal
ORDER by C.Ordinal;

GO
/****** Object:  StoredProcedure [dev].[HelpSchemata]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- EXEC [dev].[HelpSchemata]
CREATE PROC [dev].[HelpSchemata]
 @Tag sysname = N'MS_Description'
AS
SELECT
  s.name Name
, s.schema_id Id
, s.principal_id [Owner]
, x.name PropertyName
, x.value PropertyValue
FROM sys.schemas s
CROSS APPLY fn_listextendedproperty (@Tag, N'SCHEMA', s.name, null, null, null, null) x

GO
/****** Object:  StoredProcedure [dev].[HelpTables]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- EXEC dev.HelpTables N'[etl].[SubmissionV0]'
-- EXEC dev.HelpTables
-- EXEC dev.HelpTables N'dev.%'
-- EXEC dev.HelpTables @TablesOnly = 0
CREATE PROC [dev].[HelpTables]
  @TableName sysname = N'%.%'
, @TablesOnly bit = 1
AS
DECLARE @Yes bit = 1, @No bit = 0;
SELECT
  C.TableId
, C.TableSchemaName
, C.TableName
, C.TableCreated
, C.TableModified
, C.Ordinal
, C.ColumnName
, IsNull(C.UserTypeName + ' ', '') + IsNull(C.TypeDesignator, '') AS Domain
, CASE C.IsNullable
    WHEN 1 THEN @No
    WHEN 0 THEN @Yes
  END IsMandatory
, C.IsComputed
, C.IsIdentity
, X.[value] Explanation
, X.[name] XpName
, X.major_id XpMajor_id
, X.minor_id XpMinor_id
FROM dev.iColumn(OBJECT_ID(@TableName)) C
LEFT JOIN sys.extended_properties X
   ON X.major_id = C.TableId
  AND X.class = 1
  AND X.minor_id = C.Ordinal
WHERE C.TableSchemaName LIKE PARSENAME(@TableName, 2)
  AND C.TableName LIKE PARSENAME(@TableName, 1)
  AND (X.minor_id = 0 OR X.minor_id Is Null OR @TablesOnly = 0)

RETURN 0;

GO
/****** Object:  StoredProcedure [dev].[StringOccurence]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dev].[StringOccurence]
  @Pattern varchar(200),
  @Mode    int = 0
AS
-- [Develop].[StringOccurence] 'while', 1
SELECT @Pattern = '%' + @Pattern + '%'

SELECT
  SCHEMA_NAME(o.schema_id) [Schema],
  O.[name],
  O.[object_id],
  O.[type],
  --C.colid, C.number, C.colid,
  CASE WHEN (@Mode & 1 = 1) THEN M.[definition] ELSE Null END Definition
FROM       sys.all_objects O
INNER join sys.sql_modules M
  ON  O.[object_id] = M.[object_id]
  AND M.[definition] LIKE @Pattern escape '!'
--sp_helptext 'sp_helptext'

GO
/****** Object:  StoredProcedure [etl].[BulkLoadBlob]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [etl].[BulkLoadBlob]
  @FilePath dbo.URL,
  @Chunk varbinary(max) = Null output
AS
DECLARE
  @X int,
  @Sentence nvarchar(1000);

SELECT
  @Sentence = 'SELECT @x = BulkColumn FROM OPENROWSET(BULK '''
  + @FilePath + ''', SINGLE_BLOB) AS x';
  
EXEC sys.sp_executesql @Sentence, N'@x varbinary(max) output', @Chunk output;
SELECT @X = @@ERROR;
IF @X <> 0 RETURN @X;
IF @Chunk is Null return -1;
RETURN 0;

GO
/****** Object:  StoredProcedure [etl].[RegisterSubmission]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [etl].[RegisterSubmission]
  @Submission int = Null OUTPUT
, @Impersonation sysname = Null
, @InjectorCaption dbo.TxCaption
, @Species dbo.TxCaption
, @SourceFolder dbo.URL = Null
, @FileName dbo.URL = Null
, @Superior bigint = Null
, @RowLo bigint = Null
, @RowHi bigint = Null
, @Year smallint = Null
, @Month tinyint = Null
, @At datetime = Null
AS
SET NOCOUNT ON;
DECLARE @X int, @Rows int;

SELECT @At = IsNull(@At, GETDATE());

INSERT etl.SubmissionV0(SourceFolder, [FileName], InjectorCaption, Impersonation, Species, Pertains, Superior, RowLo, RowHi)--YM,
  VALUES (@SourceFolder, @FileName, @InjectorCaption, @Impersonation, @Species, @At, @Superior, @RowLo, @RowHi);--@YM
SELECT @X = @@ERROR, @Rows = @@ROWCOUNT, @Submission = @@IDENTITY;

RETURN @X;

GO
/****** Object:  StoredProcedure [etl].[TransitSubmission]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [etl].[TransitSubmission]
  @Submission bigint
, @Status int = 0
AS
SET NOCOUNT ON;
IF @Submission Is Null BEGIN
  RAISERROR('Bad invocation!', 11, 1);
  RETURN -1;
END
SELECT @Status = IsNull(@Status, 0);

SELECT CAST(Id as bigint) Id, [Action], [Status]
INTO #Was
FROM [etl].[Submission]
WHERE 1=0;

DECLARE
  @Rows int, @X int
, @Action bigint
, @Context sysname = (SELECT SCHEMA_NAME(schema_id)  + NCHAR(46) + OBJECT_NAME([object_id]) FROM sys.all_objects WHERE object_id = @@PROCID)
;

INSERT ssm.[Action] (Context, Impersonation)
  VALUES (@Context, suser_sname());
SELECT @Action = @@IDENTITY;

UPDATE [etl].[Submission] SET
  [Status] = @Status
, [Action] = @Action
OUTPUT deleted.Id, deleted.[Action], deleted.[Status] INTO #Was
WHERE Id = @Submission AND [Status] <> @Status;
SELECT @X = @@ERROR, @Rows = @@ROWCOUNT;

IF @X <> 0 RETURN @X;
IF @Rows = 0 BEGIN
  UPDATE ssm.[Action] SET Details = '<Operation Diagnose="denied" />' WHERE Id = @Action;
  RETURN -2;
END

INSERT [etl].[SubmissionTransition] (Submission, ActionWas, ActionIs, StatusWas, StatusIs)
  SELECT Id, [Action], @Action, [Status], @Status FROM #Was;
RETURN @@ERROR;

GO
/****** Object:  StoredProcedure [ssm].[LogError]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ssm].[LogError] 
  @LogEntryId [int] = 0 OUTPUT  -- Contains the Log entry Id of the error registered
AS
SET NOCOUNT ON;
SET @LogEntryId = 0; -- value of 0 indicates that error information was not logged

BEGIN TRY
    IF ERROR_NUMBER() IS NULL
        RETURN; -- Return if there is no error information to log

    IF XACT_STATE() = -1 -- Data insertion/modification is not allowed when a transaction is in an uncommittable state.
    BEGIN
        PRINT 'Cannot log error since the current transaction is in an uncommittable state. ' 
            + 'Rollback the transaction before executing LogError in order to successfully log error information.';
        RETURN;
    END;

    INSERT ssm.Error
    (   Principal, 
        Number, 
        Severity, 
        State, 
        Context, 
        ErrorLine, 
        ErrorMessage,
	[Login]
     ) 
    VALUES 
    (   CONVERT(sysname, CURRENT_USER), 
        ERROR_NUMBER(),
        ERROR_SEVERITY(),
        ERROR_STATE(),
        ERROR_PROCEDURE(),
        ERROR_LINE(),
        ERROR_MESSAGE(),
	CONVERT(sysname, SUSER_SNAME())
    );

    SELECT @LogEntryId = @@IDENTITY;
END TRY
BEGIN CATCH
    PRINT 'An error occurred WHILE trying to register an error!';
    EXECUTE [ssm].[PrintError];
    RETURN -1;
END CATCH

GO
/****** Object:  StoredProcedure [ssm].[PrintError]    Script Date: 11/22/2016 1:59:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ssm].[PrintError] 
AS
SET NOCOUNT ON;
PRINT 'Error ' + CONVERT(varchar(50), ERROR_NUMBER()) +
      ', Severity ' + CONVERT(varchar(5), ERROR_SEVERITY()) +
      ', State ' + CONVERT(varchar(5), ERROR_STATE()) + 
      ', Procedure ' + ISNULL(ERROR_PROCEDURE(), '-') + 
      ', Line ' + CONVERT(varchar(5), ERROR_LINE());
PRINT ERROR_MESSAGE();

GO
/****** Object:  DdlTrigger [tr_DDL_DATABASE_LEVEL_EVENTS]    Script Date: 11/22/2016 1:59:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [tr_DDL_DATABASE_LEVEL_EVENTS] 
ON DATABASE 
FOR DDL_DATABASE_LEVEL_EVENTS 
AS
SET NOCOUNT ON

DECLARE
  @data XML = EVENTDATA()
, @schema sysname
, @object sysname
, @eventType sysname
, @target sysname;

SELECT
  @eventType = @data.value('(/EVENT_INSTANCE/EventType)[1]', 'sysname')
, @schema = @data.value('(/EVENT_INSTANCE/SchemaName)[1]', 'sysname')
, @object = @data.value('(/EVENT_INSTANCE/ObjectName)[1]', 'sysname'); 

SELECT
  @object = CASE WHEN @eventType NOT LIKE '%SCHEMA%' THEN @object END
, @target = @schema +  ISNULL('.' + @object, '');
PRINT '  ' + @eventType + ' - ' + @target;

IF @eventType IS NULL
    PRINT CONVERT(nvarchar(max), @data);

INSERT [ssm].[DDL_Issue]
(
  Posted
, DbUser
, Impersonation
, SystemUser
, [Event]
, [Target]
, Commands
, [Details]
, Host
, [Application]
) 
VALUES 
(
  GETDATE()
, USER_NAME()
, SUSER_SNAME()
, ORIGINAL_LOGIN()
, @eventType
, CONVERT(sysname, @target)
, @data.value('(/EVENT_INSTANCE/TSQLCommand)[1]', 'nvarchar(max)')
, @data
, HOST_NAME()
, PROGRAM_NAME()
);

GO
ENABLE TRIGGER [tr_DDL_DATABASE_LEVEL_EVENTS] ON DATABASE
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Access Management' , @level0type=N'SCHEMA',@level0name=N'acm'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AviaCenter ETL Raw Data, Search Cache' , @level0type=N'SCHEMA',@level0name=N'acrawcache'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Development Tools' , @level0type=N'SCHEMA',@level0name=N'dev'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Expedia (EAN)' , @level0type=N'SCHEMA',@level0name=N'ean'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Expedia (EAN) ETL Raw Data' , @level0type=N'SCHEMA',@level0name=N'eanraw'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Extract, Transform, Load' , @level0type=N'SCHEMA',@level0name=N'etl'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Geography Objects' , @level0type=N'SCHEMA',@level0name=N'geo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IATA codes' , @level0type=N'SCHEMA',@level0name=N'iata'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ISO codes' , @level0type=N'SCHEMA',@level0name=N'iso'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Source of Data to Populate Tables' , @level0type=N'SCHEMA',@level0name=N'pop'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Search Audit and Analysis' , @level0type=N'SCHEMA',@level0name=N'sau'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'System State Management' , @level0type=N'SCHEMA',@level0name=N'ssm'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Standing Data & Master Data' , @level0type=N'SCHEMA',@level0name=N'std'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Temporary Stubs and Tools' , @level0type=N'SCHEMA',@level0name=N'tmp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'General Purpose Utilities' , @level0type=N'SCHEMA',@level0name=N'uti'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Tools & Studies' , @level0type=N'SCHEMA',@level0name=N'xxx'
GO
USE [master]
GO
ALTER DATABASE [UpstreamDataSupply] SET  READ_WRITE 
GO
