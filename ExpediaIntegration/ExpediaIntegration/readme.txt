Steps:

1) install Commons assembly to GAC.  
    gacutil -i Commons.dll   
   example 
  (C:\Program Files (x86)\Microsoft SDKs\Windows\v8.1A\bin\NETFX 4.5.1 Tools\gacutil -i c:\temp\Commons.dll)

2) execute  download-and-extract package passing to it parameters via command line
 
    dtexec /f "DownloadPackage.dtsx" /Set "\DownloadPackage.Variables[User::WorkDir].Properties[Value];";"c:\temp"
	/Set "\DownloadPackage.Variables[User::EanURL].Properties[Value];";"https://www.ian.com/affiliatecenter/include/V2/ActivePropertyList.zip"

3) run one by one data transfer packages
    ActivePropertyList.dtsx
	...
	WhatToExpectList.dtsx

generate command line parameters
https://sqlstudies.com/2013/07/31/using-dtexecui-to-generate-a-dtexec-command-line-statement-the-easy-way/
     