﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Parsers
{
    public static class FloatParser
    {
        public static float Parse(this string value, float @default)
        {
            try
            {
                if (value != null)
                    value = value.Replace(",", ".");
                return float.Parse(value,CultureInfo.InvariantCulture);
            }
            catch (Exception exception)
            {

            }
            return @default;
        }

    }
}
