﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Parsers
{
    public static class IntegerParser
    {
        public static int Parse(this string value,int @default)
        {
            try
            {
                return int.Parse(value);
            }
            catch (Exception exception)
            {
                
            }
            return @default;
        }

        public static long Parse(this string value, long @default)
        {
            try
            {
                return long.Parse(value);
            }
            catch (Exception exception)
            {

            }
            return @default;
        }

    }
}
