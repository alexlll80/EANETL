﻿using Commons.Compress;
using Commons.Files;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Commons
{
    public class DownloadHelper
    {
        private string workDir = string.Empty;
        public DownloadHelper(string path)
        {
            workDir = path;
        }

        public string DownloadAndExtractFile(string url)
        {
            return DownloadAndExtractFile(url,Path.GetFileNameWithoutExtension(url));
        }

        public string DownloadAndExtractFile(string url,string fileName)
        {
            DiskHelper.CreateFolderIfNotExist(workDir);

            var client = new WebClient();

            var path = GetArchiveFilePath(fileName);
            var folder = GetUnzippedFolderPath(fileName);

            DiskHelper.CreateFolderIfNotExist(folder);
            DiskHelper.DeleteFileIfExist(path);

            client.DownloadFile(url, path);
            ExtractFile(folder, path);

            return DiskHelper.FindFileWithoutExtension(folder, fileName);
        }

        private string GetUnzippedFolderPath(string fileName)
        {
            return Path.Combine(workDir, fileName);
        }

        private string GetArchiveFilePath(string fileName)
        {
            return Path.Combine(workDir, fileName + ".zip");
        }

        private string ExtractFile(string folder, string fileName)
        {
            ZipHelper.Extract(fileName, folder);
            return Path.GetFileNameWithoutExtension(fileName);
        }
    }
}
