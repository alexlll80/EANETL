﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Files
{
    public class DiskHelper
    {
        public static void CreateFolderIfNotExist(string folder)
        {
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
        }

        public static void DeleteFileIfExist(string fileName)
        {
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }
        }

        public static string FindFileWithoutExtension(string folder,string fileName)
        {
            return Directory.GetFiles(folder, fileName+".*", SearchOption.TopDirectoryOnly).FirstOrDefault();
        }
    }
}
