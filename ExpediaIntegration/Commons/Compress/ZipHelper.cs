﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Compress
{
    public class ZipHelper
    {
        public static void Decompress(FileInfo fi)
        {
            using (FileStream inFile = fi.OpenRead())
            {
                string curFile = fi.FullName;
                string origName = curFile.Remove(curFile.Length - fi.Extension.Length);

                using (FileStream outFile = File.Create(origName))
                {
                    using (GZipStream Decompress = new GZipStream(inFile, CompressionMode.Decompress))
                    {
                        Decompress.CopyTo(outFile);
                    }
                }
            }
        }

        public static void Extract(string zipFilePath, string toDir)
        {
            if (!Directory.Exists(toDir))
            {
                Directory.CreateDirectory(toDir);
            }
            using (ZipArchive archive = ZipFile.OpenRead(zipFilePath))
            {
                foreach (ZipArchiveEntry entry in archive.Entries)
                {
                    var fileName = Path.Combine(toDir, entry.FullName);
                    if (File.Exists(fileName))
                    {
                        File.Delete(fileName);
                    }
                    entry.ExtractToFile(fileName);
                }
            }
        }
    }
}
